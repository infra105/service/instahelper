# skillbox-diploma
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Предварительные настройки
Прежде чем запустить приложение, необходимо запустить инфраструктуру. Перейдите по следующим ссылкам и выполните все по инструкции:
1. https://gitlab.com/infra105/infras/terraform/webserver-cluster-elb
2. https://gitlab.com/infra105/infras/ansible/nginx_proxy
3. https://gitlab.com/infra105/infras/ansible/docker
4. https://gitlab.com/infra105/infras/ansible/gitlab-runner

Cкопируйте репозиторий для редактирования:
* git clone https://gitlab.com/infra105/service/instahelper.git

Сделайте все необходимые изменения и затем выполните команду:
* git remote add origin https://gitlab.com/infra105/service/instahelper.git
* git add . && git commit && git push -uf origin developer

